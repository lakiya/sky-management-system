/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.service;

import com.sky.entity.Customer;
import com.sky.respository.CustomerRepository;
import java.util.List;

/**
 *
 * @author Lakshitha
 */
public class CustomerService {
    
    public void saveCustomer(Customer customer){
        
        CustomerRepository customerRepository = new CustomerRepository();
        customerRepository.saveCustomer(customer);
    }
    
    public void updateCustomer(Customer customer){
        customer.setFullName(getFullName(customer));
        
        CustomerRepository customerRepository = new CustomerRepository();
        customerRepository.updateCustomer(customer);
    }
    
    public List<Customer> getAllCustomers(){
        CustomerRepository customerRepository = new CustomerRepository();
        return customerRepository.getAllCustomers();
    }
    
    private String getFullName(Customer customer){
        return customer.getFirstName() + " " + customer.getLastName();
    }
}
